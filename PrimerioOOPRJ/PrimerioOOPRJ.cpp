// PrimerioOOPRJ.cpp : main project file.

#include "stdafx.h"

using namespace System;


//To declare a class in C++, you use the keywords ref class
ref class Animal {
public :
	int legs;
	void setName(String ^nm) {
		name = nm;
    }
	String^ getName() {
		return name;
	}
private :
	String ^name;
};

int main()
{
	Animal cat, dog;
	cat.legs = 4;
	cat.setName("Bill");

	dog.legs = 4;
	dog.setName("Dog");

	Console::WriteLine("Name: " + cat.getName() + " Legs: " + cat.legs);
	Console::WriteLine("Name: " + dog.getName() + " Legs: " + dog.legs);

	Console::Read();
    return 0;
}
